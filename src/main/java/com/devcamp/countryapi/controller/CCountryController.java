package com.devcamp.countryapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CCountry;
import com.devcamp.countryapi.repository.ICountryRepository;

@CrossOrigin
@RestController
public class CCountryController {
    @Autowired
    ICountryRepository pICountryRepository;

    //API để lấy thông tin toàn bộ country
    @GetMapping("/countries")
    public ResponseEntity<List<CCountry>> getAllCountry() {
        try {
            List<CCountry> listAllCountries = new ArrayList<>();
            pICountryRepository.findAll().forEach(listAllCountries::add);
            return new ResponseEntity<>(listAllCountries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để lấy thông tin của 1 country thông qua ID
    @GetMapping("/country/{id}")
    public ResponseEntity<CCountry> getCountryById(@PathVariable("id") long id) {
        try {
            Optional<CCountry> country = pICountryRepository.findById(id);
            if(country.isPresent()) {
                return new ResponseEntity<>(country.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API để tạo mới 1 country 
    @PostMapping("/countries")
    public ResponseEntity<CCountry> createCountry(@RequestBody CCountry pCountry) {
        try {
            CCountry country = new CCountry();
            country.setCountryCode(pCountry.getCountryCode());
            country.setCountryName(pCountry.getCountryName());
            CCountry savedCountry = pICountryRepository.save(country);
            return new ResponseEntity<>(savedCountry, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để sửa thông tin 1 country
    @PutMapping("/country/{id}")
    public ResponseEntity<CCountry> updateCountry(@RequestBody CCountry pCountry, @PathVariable("id") long id) {
        try {
            Optional<CCountry> countryData = pICountryRepository.findById(id);
            if(countryData.isPresent()) {
                CCountry country = countryData.get();
                country.setCountryName(pCountry.getCountryName());
                country.setCountryCode(pCountry.getCountryCode());
                country.setRegions(pCountry.getRegions());
                CCountry savedCountry = pICountryRepository.save(country);
                return new ResponseEntity<>(savedCountry, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API để xóa 1 country
    @DeleteMapping("/country/{id}")
    public ResponseEntity<CCountry> deleteCountryById(@PathVariable("id") long id) {
        try {
            pICountryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
