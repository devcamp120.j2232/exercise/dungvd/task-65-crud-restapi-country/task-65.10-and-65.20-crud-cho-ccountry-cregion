package com.devcamp.countryapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CRegion;
import com.devcamp.countryapi.repository.ICountryRepository;
import com.devcamp.countryapi.repository.IRegionRepository;

@RestController
@CrossOrigin
public class CRegionController {
    @Autowired
    IRegionRepository pIRegionRepository;

    @Autowired
    ICountryRepository pICountryRepository;

    //API để lấy thông tin toàn bộ region
    @GetMapping("/regions")
    public ResponseEntity<List<CRegion>> getAllRegion() {
        try {
            List<CRegion> listAllRegions = new ArrayList<>();
            pIRegionRepository.findAll().forEach(listAllRegions::add);
            return new ResponseEntity<>(listAllRegions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để lấy thông tin của 1 region thông qua ID
    @GetMapping("/region/{id}")
    public ResponseEntity<CRegion> getRegionById(@PathVariable("id") long id) {
        try {
            Optional<CRegion> region = pIRegionRepository.findById(id);
            if(region.isPresent()) {
                return new ResponseEntity<>(region.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //API để tạo mới 1 region
    @PostMapping("/regions")
    public ResponseEntity<CRegion> createRegion(@RequestBody CRegion pRegion) {
        try {
            CRegion region = new CRegion();
            region.setRegionCode(pRegion.getRegionCode());
            region.setRegionName(pRegion.getRegionName());
            CRegion savedRegion = pIRegionRepository.save(region);
            return new ResponseEntity<>(savedRegion, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để sửa thông tin 1 region
    @PutMapping("/region/{id}")
    public ResponseEntity<CRegion> updateRegion(@RequestBody CRegion pRegion, @PathVariable("id") long id) {
        try {
            Optional<CRegion> regionData = pIRegionRepository.findById(id);
            if(regionData.isPresent()) {
                CRegion region = regionData.get();
                region.setRegionCode(pRegion.getRegionCode());
                region.setRegionName(pRegion.getRegionName());
                region.setCountry(pRegion.getCountry());
                CRegion savedRegion = pIRegionRepository.save(region);
                return new ResponseEntity<>(savedRegion, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // API để xóa 1 reion
    @DeleteMapping("/region/{id}")
    public ResponseEntity<CRegion> deleteRegion(@PathVariable("id") long id) {
        try {
            pIRegionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra toàn bộ region của 1 country
    @GetMapping("/country/{id}/region")
    public ResponseEntity<List<CRegion>> getRegionsByCountryId(@PathVariable("id") long countryId) {
        try {
            List<CRegion> listRegionOfCountry = pICountryRepository.findById(countryId).get().getRegions();
            return new ResponseEntity<>(listRegionOfCountry, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
