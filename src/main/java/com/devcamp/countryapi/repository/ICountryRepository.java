package com.devcamp.countryapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryapi.model.CCountry;

public interface ICountryRepository extends JpaRepository<CCountry, Long>{
    CCountry findByCountryCode(String countryCode);
}
